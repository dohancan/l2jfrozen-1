package com.l2jfrozen.configuration;

public class ThreadConfig implements ConfigManagerObserver {
    // Threads
    public static int THREAD_P_EFFECTS;
    public static int THREAD_P_GENERAL;
    public static int GENERAL_PACKET_THREAD_CORE_SIZE;
    public static int IO_PACKET_THREAD_CORE_SIZE;
    public static int GENERAL_THREAD_CORE_SIZE;
    public static int AI_MAX_THREAD;
    private static ConfigManager configManager;
    private static ThreadConfig networkConfig;

    private ThreadConfig() {
        configManager = ConfigManager.getInstance();

    }

    public static ThreadConfig getInstance() {
        if (networkConfig == null)
            networkConfig = new ThreadConfig();
        return networkConfig;
    }

    @Override
    public void configurationLoad() {
        THREAD_P_EFFECTS = configManager.getInteger("ThreadPoolSizeEffects");
        THREAD_P_GENERAL = configManager.getInteger("ThreadPoolSizeGeneral");
        GENERAL_PACKET_THREAD_CORE_SIZE = configManager.getInteger("GeneralPacketThreadCoreSize");
        IO_PACKET_THREAD_CORE_SIZE = configManager.getInteger("UrgentPacketThreadCoreSize");
        AI_MAX_THREAD = configManager.getInteger("AiMaxThread");
        GENERAL_THREAD_CORE_SIZE = configManager.getInteger("GeneralThreadCoreSize");
    }
}