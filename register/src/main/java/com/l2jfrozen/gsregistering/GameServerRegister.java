package com.l2jfrozen.gsregistering;

import com.l2jfrozen.LoggerManager;
import com.l2jfrozen.configuration.ConfigManager;
import com.l2jfrozen.configuration.DatabaseConfig;
import com.l2jfrozen.database.GameServerInstanceManager;
import com.l2jfrozen.database.manager.impl.GameServerManagerImpl;
import com.l2jfrozen.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.sql.SQLException;
import java.util.Map;

public class GameServerRegister {
    private static final Logger LOGGER = LoggerFactory.getLogger(GameServerRegister.class);
    private static String _choice;

    public static void main(String[] args) throws IOException, SQLException {
        LoggerManager.getInstance();
        ConfigManager.getInstance().nativeReloadConfig();
        LineNumberReader _in = new LineNumberReader(new InputStreamReader(System.in));

        LOGGER.info("Welcome to GameServer Registering");
        LOGGER.info("Enter The id of the server you want to register");
        LOGGER.info("Type 'help' to get a list of ids.");
        LOGGER.info("Type 'clean' to unregistered all currently registered gameservers on this LoginServer.");
        while (true) {
            LOGGER.info("Your choice:");
            _choice = _in.readLine();
            if (_choice.equalsIgnoreCase("help")) {
                for (Map.Entry<Integer, String> entry : GameServerManagerImpl.getInstance().getServerNames().entrySet()) {
                    LOGGER.error("Server: ID: " + entry.getKey() + "\t- " + entry.getValue() + " - In Use: " + (GameServerManagerImpl.getInstance().isRegisterServer(entry.getKey()) ? "YES" : "NO"));
                }
                LOGGER.error("You can also see servername.xml");
            } else if (_choice.equalsIgnoreCase("clean")) {
                System.out.print("This is going to UNREGISTER ALL servers from this LoginServer. Are you sure? (y/n) ");
                _choice = _in.readLine();
                if (_choice.equals("y")) {
                    GameServerManagerImpl.getInstance().clear();
                } else {
                    LOGGER.error("ABORTED");
                }
            } else {
                try {
                    int id = Integer.parseInt(_choice);
                    int size = GameServerManagerImpl.getInstance().getServerNames().size();
                    if (size == 0) {
                        LOGGER.info("No server names avalible, please make sure that servername.xml is in the LoginServer directory.");
                        System.exit(1);
                    }

                    _choice = "";


                    while (!_choice.equalsIgnoreCase("")) {
                        LOGGER.error("External Server Ip:");
                        _choice = _in.readLine();
                    }

                    String ip = _choice;

                    String name = GameServerManagerImpl.getInstance().getServerNames().get(id);
                    if (name == null) {
                        LOGGER.error("No name for id: " + id);
                        continue;
                    }

                    if (GameServerManagerImpl.getInstance().isRegisterServer(id)) {
                        LOGGER.error("This id is not free");
                    } else {
                        String hex = StringUtil.generateHex();
                        GameServerManagerImpl.getInstance().add(id, ip, hex);
                        StringUtil.saveHexid(id, hex);
                        LOGGER.info("Server Registered hexid saved to 'hexid.properties'");
                        LOGGER.info("Put this file in the /config folder of your gameserver.");
                        return;
                    }
                } catch (NumberFormatException nfe) {
                    LOGGER.error("Please, type a number or 'help'");
                }
            }
        }
    }
}