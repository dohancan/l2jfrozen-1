/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfrozen.gameserver.handler.voicedcommandhandlers;

import com.l2jfrozen.configuration.GameServerConfig;
import com.l2jfrozen.gameserver.handler.IVoicedCommandHandler;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfrozen.gameserver.network.serverpackets.ItemList;

/**
 * This class trades Gold Bars for Adena and vice versa.
 *
 * @author Ahmed
 */
public class BankingCmd implements IVoicedCommandHandler {
    private static String[] _voicedCommands =
            {
                    "bank",
                    "withdraw",
                    "deposit"
            };

    /**
     * @see com.l2jfrozen.gameserver.handler.IVoicedCommandHandler#useVoicedCommand(java.lang.String, com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance, java.lang.String)
     */
    @Override
    public boolean useVoicedCommand(String command, L2PcInstance activeChar, String target) {

        if (!activeChar.getClient().getFloodProtectors().getTransaction().tryPerformAction("bank")) {
            activeChar.sendMessage("You Cannot Use The Banking System So Fast!");
            return true;
        }

        if (command.equalsIgnoreCase("bank")) {
            activeChar.sendMessage(".deposit (" + GameServerConfig.BANKING_SYSTEM_ADENA + " Adena = " + GameServerConfig.BANKING_SYSTEM_GOLDBARS + " Goldbar) / .withdraw (" + GameServerConfig.BANKING_SYSTEM_GOLDBARS + " Goldbar = " + GameServerConfig.BANKING_SYSTEM_ADENA + " Adena)");
        } else if (command.equalsIgnoreCase("deposit")) {
            if (activeChar.getInventory().getInventoryItemCount(57, 0) >= GameServerConfig.BANKING_SYSTEM_ADENA) {
                activeChar.getInventory().reduceAdena("Goldbar", GameServerConfig.BANKING_SYSTEM_ADENA, activeChar, null);
                activeChar.getInventory().addItem("Goldbar", 3470, GameServerConfig.BANKING_SYSTEM_GOLDBARS, activeChar, null);
                activeChar.getInventory().updateDatabase();
                activeChar.sendPacket(new ItemList(activeChar, true));
                activeChar.sendMessage("Thank you, now you have " + GameServerConfig.BANKING_SYSTEM_GOLDBARS + " Goldbar(s), and " + GameServerConfig.BANKING_SYSTEM_ADENA + " less adena.");
            } else {
                activeChar.sendMessage("You do not have enough Adena to convert to Goldbar(s), you need " + GameServerConfig.BANKING_SYSTEM_ADENA + " Adena.");
            }
        } else if (command.equalsIgnoreCase("withdraw")) {
            // If player hasn't enough space for adena
            long a = activeChar.getInventory().getInventoryItemCount(57, 0);
            long b = GameServerConfig.BANKING_SYSTEM_ADENA;
            if (a + b > Integer.MAX_VALUE) {
                activeChar.sendMessage("You do not have enough space for all the adena in inventory!");
                return false;
            }

            if (activeChar.getInventory().getInventoryItemCount(3470, 0) >= GameServerConfig.BANKING_SYSTEM_GOLDBARS) {
                activeChar.getInventory().destroyItemByItemId("Adena", 3470, GameServerConfig.BANKING_SYSTEM_GOLDBARS, activeChar, null);
                activeChar.getInventory().addAdena("Adena", GameServerConfig.BANKING_SYSTEM_ADENA, activeChar, null);
                activeChar.getInventory().updateDatabase();
                activeChar.sendPacket(new ItemList(activeChar, true));
                activeChar.sendMessage("Thank you, now you have " + GameServerConfig.BANKING_SYSTEM_ADENA + " Adena, and " + GameServerConfig.BANKING_SYSTEM_GOLDBARS + " less Goldbar(s).");
            } else {
                activeChar.sendMessage("You do not have any Goldbars to turn into " + GameServerConfig.BANKING_SYSTEM_ADENA + " Adena.");
            }
        }
        return true;
    }

    /**
     * @see com.l2jfrozen.gameserver.handler.IVoicedCommandHandler#getVoicedCommandList()
     */
    @Override
    public String[] getVoicedCommandList() {
        return _voicedCommands;
    }
}