
package com.l2jfrozen.gameserver.handler.skillhandlers;

import com.l2jfrozen.gameserver.handler.ISkillHandler;
import com.l2jfrozen.gameserver.handler.SkillHandler;
import com.l2jfrozen.gameserver.model.L2Character;
import com.l2jfrozen.gameserver.model.L2Skill;
import com.l2jfrozen.gameserver.model.L2Skill.SkillType;
import com.l2jfrozen.gameserver.network.SystemMessageId;
import com.l2jfrozen.gameserver.network.serverpackets.StatusUpdate;
import com.l2jfrozen.gameserver.network.serverpackets.SystemMessage;
import com.l2jfrozen.util.GArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CombatPointHeal implements ISkillHandler<L2Character> {
    private static final Logger LOGGER = LoggerFactory.getLogger(CombatPointHeal.class.getName());
    private static final SkillType[] SKILL_IDS = {SkillType.COMBATPOINTHEAL, SkillType.COMBATPOINTPERCENTHEAL};

    @Override
    public <A extends L2Character> void useSkill(A activeChar, L2Skill skill, GArray<L2Character> targets) {
        //check for other effects
        try {
            ISkillHandler handler = SkillHandler.getInstance().getSkillHandler(SkillType.BUFF);

            if (handler != null) {
                handler.useSkill(activeChar, skill, targets);
            }
        } catch (Exception e) {
            LOGGER.error("", e);
        }

        for (L2Character target : targets) {

            double cp = skill.getPower();
            if (skill.getSkillType() == SkillType.COMBATPOINTPERCENTHEAL) {
                cp = target.getMaxCp() * cp / 100.0;
            }
            SystemMessage sm = new SystemMessage(SystemMessageId.S1_CP_WILL_BE_RESTORED);
            sm.addNumber((int) cp);
            target.sendPacket(sm);

            target.setCurrentCp(cp + target.getCurrentCp());
            StatusUpdate sump = new StatusUpdate(target.getObjectId());
            sump.addAttribute(StatusUpdate.CUR_CP, (int) target.getCurrentCp());
            target.sendPacket(sump);
        }
    }

    @Override
    public SkillType[] getSkillIds() {
        return SKILL_IDS;
    }
}
