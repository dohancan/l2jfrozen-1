package com.l2jfrozen.database.connection;

import com.l2jfrozen.configuration.DatabaseConfig;
import com.l2jfrozen.database.BaseTest;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * User: vdidenko
 * Date: 27.11.13
 * Time: 19:51
 */
public class DatabaseControllerTest extends BaseTest {
    DatabaseController databaseController;
    @Before
    public void setUp() throws Exception {

    }

    @After
    public void setDown() throws Exception {

    }

    @Test
    public void testClose() throws Exception {

    }

    @Test
    public void testGetConnection() throws Exception {


    }

    @Test
    public void testGetConnectionThread() throws Exception {


    }
}
