package com.l2jfrozen.database.dao.manager;

import com.l2jfrozen.database.model.GameServer;

import java.util.List;

/**
 * User: vdidenko
 * Date: 26.11.13 *
 * Time: 23:02
 */
public interface GameServerManagerDAO {

    List<GameServer> getAll();

    public GameServer get(Integer id);

    public GameServer add(GameServer gameServer);

    public void clear();
}
