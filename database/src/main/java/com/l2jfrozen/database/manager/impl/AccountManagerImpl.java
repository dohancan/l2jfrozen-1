package com.l2jfrozen.database.manager.impl;

import com.l2jfrozen.database.dao.manager.impl.AccountManagerDAOImpl;
import com.l2jfrozen.database.exception.WrongAccountCredentialException;
import com.l2jfrozen.database.manager.AccountManager;
import com.l2jfrozen.database.model.Account;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;

/**
 * vdidenko
 * 07.12.13
 */
public class AccountManagerImpl implements AccountManager {
    protected static final Logger LOGGER = LoggerFactory.getLogger(AccountManagerImpl.class);

    private static AccountManager accountManager;

    public static AccountManager getInstance() {
        if (accountManager == null) {
            accountManager = new AccountManagerImpl();
        }
        return accountManager;
    }

    @Override
    public Account get(String login, String password) throws WrongAccountCredentialException {
        try {
            Account account=AccountManagerDAOImpl.getInstance().get(login);
            if(account!=null && !account.getPassword().equals(password)){
                throw new WrongAccountCredentialException("Wrong login or password");
            }
            return account;
        } catch (SQLException e) {
            LOGGER.error("Account load fault", e);
        }
        return null;
    }

    @Override
    public Account create(String login, String password, int lastServer, int accessLevel, String lastIp) {
        Account account = new Account();
        account.setLogin(login);
        account.setPassword(password);
        account.setAccessLevel(accessLevel);
        account.setLastServer(lastServer);
        account.setLastIp(lastIp);
        try {
            return AccountManagerDAOImpl.getInstance().add(account);
        } catch (SQLException e) {
            LOGGER.error("", e);
        }
        return null;
    }

    @Override
    public Account update(Account account) {
        return AccountManagerDAOImpl.getInstance().update(account);
    }
}
