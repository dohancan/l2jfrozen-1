package com.l2jfrozen.database.manager;

import com.l2jfrozen.database.exception.WrongAccountCredentialException;
import com.l2jfrozen.database.model.Account;

/**
 * vdidenko
 * 07.12.13
 */
public interface AccountManager {
    Account get(String login, String password) throws WrongAccountCredentialException;

    Account create(String login, String password, int lastServer, int accesslevel, String lastIp);

    Account update(Account account);
}
