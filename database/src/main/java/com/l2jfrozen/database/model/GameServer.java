package com.l2jfrozen.database.model;

import java.util.ArrayList;
import java.util.List;

/**
 * User: vdidenko
 * Date: 26.11.13
 * Time: 23:01
 */
public class GameServer {
    private Integer id;
    private String hexid;
    private String host = "";
    private List<Account> accounts=new ArrayList<>();

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getHexid() {
        return hexid;
    }

    public void setHexid(String hexid) {
        this.hexid = hexid;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }
}
