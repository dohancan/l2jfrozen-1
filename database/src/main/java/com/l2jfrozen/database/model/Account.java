package com.l2jfrozen.database.model;

/**
 * vdidenko
 * 05.12.13
 */
public class Account {
    private String login;
    private String password;
    private float lastActive;
    private int accessLevel;
    private String lastIp;
    private int lastServer;
    private GameServer gameServer;

    public GameServer getGameServer() {
        return gameServer;
    }

    public void setGameServer(GameServer gameServer) {
        this.gameServer = gameServer;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public float getLastActive() {
        return lastActive;
    }

    public void setLastActive(float lastActive) {
        this.lastActive = lastActive;
    }

    public int getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(int accessLevel) {
        this.accessLevel = accessLevel;
    }

    public String getLastIp() {
        return lastIp;
    }

    public void setLastIp(String lastIp) {
        this.lastIp = lastIp;
    }

    public int getLastServer() {
        return lastServer;
    }

    public void setLastServer(int lastServer) {
        this.lastServer = lastServer;
    }
}
