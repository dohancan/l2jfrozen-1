@echo off
title L2J-Frozen: Register GameServer Console
:start
echo ------------------------------
echo Starting Register GameServer...
echo Website : www.l2jfrozen.com
echo ------------------------------
echo.

@java -Djava.util.logging.config.file=console.cfg -cp lib/*;register-1.0-SNAPSHOT.jar com.l2jfrozen.gsregistering.GameServerRegister
@pause
